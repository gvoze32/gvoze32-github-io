---
title: Mirrors
links:
  - title: Cloudflare Workers Mirror
    description: Mirror dari situs utama yang disediakan melalui Cloudflare Workers.
    website: https://mirror.syafa.tech
    image: https://workers.cloudflare.com/resources/logo/logo.svg
menu:
    main: 
        weight: 4
        params:
            icon: perspective

comments: false
---